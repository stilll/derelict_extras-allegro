/*

Boost Software License - Version 1.0 - August 17th, 2003

Permission is hereby granted, free of charge, to any person or organization
obtaining a copy of the software and accompanying documentation covered by
this license (the "Software") to use, reproduce, display, distribute,
execute, and transmit the Software, and to prepare derivative works of the
Software, and to permit third-parties to whom the Software is furnished to
do so, all subject to the following:

The copyright notices in the Software and this entire statement, including
the above license grant, this restriction and the following disclaimer,
must be included in all copies of the Software, in whole or in part, and
all derivative works of the Software, unless such copies or derivative
works are solely in the form of machine-executable object code generated by
a source language processor.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

module derelict.allegro.memfile;

private
{
    import derelict.util.loader;
    import derelict.util.system;
	import core.stdc.stdint;
    import derelict.allegro.types;

    static if (Derelict_OS_Windows)
		enum libNames = "allegro_memfile-5.0.10-mt.dll, allegro-5.0.10-monolith-mt.dll";
    else static if (Derelict_OS_Mac)
        enum libNames = "../Frameworks/AllegroMemfile-5.0.framework, /Library/Frameworks/AllegroMemfile-5.0.framwork, liballegro_memfile-5.0.10.dylib, liballegro_memfile-5.0.dylib";
    else static if (Derelict_OS_Posix)
        enum libNames = "liballegro_memfile.so.5.0.10, liballegro_memfile.so.5.0";
    else
        static assert( 0, "Need to implement Allegro libNames for this operating system." );
}

extern(C)
{
	alias ALLEGRO_FILE* function (void*, int64_t, const(char)*) da_al_open_memfile;
    alias uint32_t function() da_al_get_allegro_memfile_version;
}

__gshared
{
	da_al_open_memfile al_open_memfile;
	da_al_get_allegro_memfile_version al_get_allegro_memfile_version;
}

class DerelictAllegroMemfileLoader : SharedLibLoader
{
	public this()
	{
		super (libNames);
	}

	protected override void loadSymbols()
	{
		bindFunc(cast(void**)&al_open_memfile, "al_open_memfile");
		bindFunc(cast(void**)&al_get_allegro_memfile_version, "al_get_allegro_memfile_version");
	}
}

__gshared DerelictAllegroMemfileLoader DerelictAllegroMemfile;

shared static this()
{
    DerelictAllegroMemfile = new DerelictAllegroMemfileLoader();
}
