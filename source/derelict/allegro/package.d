module derelict.allegro;

public import derelict.allegro.acodec,
	          derelict.allegro.allegro,
	          derelict.allegro.audio,
	          derelict.allegro.color,
	          derelict.allegro.font,
	          derelict.allegro.image,
	          derelict.allegro.memfile,
	          derelict.allegro.native_dialog,
	          derelict.allegro.physfs,
	          derelict.allegro.primitives, 
	          derelict.allegro.ttf;
